///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file fish.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "animal.hpp"

namespace animalfarm {

class Fish : public Animal {
public:
	enum Color scaleColor;
	float favoriteTemp;
	
	virtual const std::string speak();
	void printInfo();
};

} // namespace animalfarm

